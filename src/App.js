import "./App.css";
import React, { useState, useEffect, useMemo } from "react";
import { fetchCategories } from "./api";
import Card from "./components/Card";
import InfiniteScroll from "react-infinite-scroll-component";

const App = () => {
  const [allCategories, setAllCategories] = useState([]);
  const [selectedCategory, setSelectedCategory] = useState();
  const [pageNum, setPageNum] = useState(1);
  const [hasMore, sethasMore] = useState(true);

  // Fetch all
  useEffect(() => {
    const fetchData = async () => {
      setAllCategories(await fetchCategories(pageNum));
    };
    fetchData();
  }, [setAllCategories]);

  //InfiniteScroll
  const fetchFriends = async () => {
    return await fetchCategories(pageNum);
  };

  const fetchPage = async () => {
    const friends = await fetchFriends();
    setAllCategories([...allCategories, ...friends]);
    if (friends.length === 0 || friends.length < 20) {
      sethasMore(false);
    }
    setPageNum(pageNum + 1);
  };

  // Filter Categories
  const fetchFilteredList = () => {
    if (!selectedCategory) {
      return allCategories;
    }
    return allCategories.filter(
      (category) => category.gender === selectedCategory
    );
  };

  //Avoid repetitive function calls
  let filteredList = useMemo(fetchFilteredList, [
    selectedCategory,
    allCategories,
  ]);

  const handleCategory = (e) => {
    setSelectedCategory(e.target.value);
  };

  //Delete list
  const hundleDelete = (id) => {
    setAllCategories(allCategories.filter((item) => item.id !== id));
  };

  return (
    <div className="app">
      <div className="filter">
        <div>
          <select
            name="category_list"
            id="category_list"
            onChange={handleCategory}
          >
            <option value="">No Filter</option>
            <option value="male">Male</option>
            <option value="female">Female</option>
          </select>
        </div>
      </div>
      <InfiniteScroll
        dataLength={filteredList.length}
        next={fetchPage}
        hasMore={hasMore}
        loader={<h4>Loading...</h4>}
      >
        <div className="friend_list">
          {filteredList.map((element, id) => (
            <Card
              hundleDelete={hundleDelete}
              phone_number={element.phone_number}
              email={element.email}
              gender={element.gender}
              name={element.names}
              imageUrl={element.image}
              id={element.id}
              key={`${element.id}${id}`}
            />
          ))}
        </div>
      </InfiniteScroll>
    </div>
  );
};

export default App;
