import axios from "axios";

export const fetchCategories = async (pageNum) => {
  try {
    const {
      data: { results },
    } = await axios.get(
      `https://randomuser.me/api/?page=${pageNum}&results=20&seed=abc`
    );
    const modifieldData = results.map((category) => ({
      gender: category.gender,
      names: category.name,
      id: `${category.id.value}${
        category.name.first + -Math.floor(Math.random() * 10 + 1)
      }`,
      phone_number: category.phone,
      email: category.email,
      image: category.picture.large,
    }));
    return modifieldData;
  } catch (error) {
    console.log(error);
  }
};
