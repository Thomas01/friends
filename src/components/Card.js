import React from "react";
import "./card.css";
import Swal from "sweetalert2";

const Card = ({
  name,
  email,
  imageUrl,
  phone_number,
  gender,
  hundleDelete,
  id,
}) => {
  const infoConnect = (email, imageUrl, phone_number) => {
    Swal.fire({
      titleText: phone_number,
      text: email,
      imageUrl: imageUrl,
      width: 400,
      heightAuto: "false",
      imageWidth: 300,
      imageHeight: 200,
      imageAlt: "Custom image",
    });
  };

  return (
    <div className="card">
      <div className="close" onClick={() => hundleDelete(id)}>
        X
      </div>
      <div className="img_div">
        <img src={imageUrl} alt="freinds" className="card-img" />
      </div>
      <h4>{`${name.first} ${name.last}`}</h4>
      <p>{gender}</p>
      <button onClick={() => infoConnect(email, imageUrl, phone_number)}>
        connect
      </button>
    </div>
  );
};

export default Card;
